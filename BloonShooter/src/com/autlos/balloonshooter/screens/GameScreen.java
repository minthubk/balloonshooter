package com.autlos.balloonshooter.screens;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonFactory.DifficultyMode;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.Sounds;
import com.autlos.balloonshooter.view.GameWorld;
import com.autlos.balloonshooter.view.WorldRenderer;
import com.autlos.sgf.ScreenAbstract;

public class GameScreen extends ScreenAbstract {
	private BalloonShooter game;
	private GameWorld world;
	private WorldRenderer renderer;

	/**
	 * 
	 * @param game
	 * @param mode
	 *           difficulty mode
	 */
	public GameScreen(BalloonShooter game, DifficultyMode mode) {
		super(Assets.background);
		this.game = game;
		game.setAdsVisible(true);
		game.catchBackButton(true);
		
		world = new GameWorld(game, mode);
		renderer = new WorldRenderer(world, batch);
		
		
		if (BalloonShooter.soundState != Sounds.MUTED)
			game.sounds.startMusic(0.25f);
	}

	@Override
	public void render(float delta) {
		if (!world.isGameOver()) {
			super.render(delta);
			
			world.update(delta);
			renderer.render();
		} else {
			// I stopMusic() because it has different volume and to start the music again 
			Sounds.stopMusic();
			game.setScreen(new GameOverScreen(world));
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		world.dispose();
		renderer.dispose();
	}

}
