package com.autlos.balloonshooter.screens;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.models.ui.MenuButton;
import com.autlos.sgf.BasicLabel;
import com.autlos.sgf.GameAbstract;
import com.autlos.sgf.ScreenAbstract;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * This class is never used since the implementation of Swarm
 * 
 * @author Aitor
 * 
 */
public class HighScoresScreen extends ScreenAbstract {
	private BalloonShooter game;

	private BasicLabel labelHighScoreEasy;
	private BasicLabel labelHighScoreNormal;
	private BasicLabel labelHighScoreHard;
	private BasicLabel labelContBalloons;
	private BasicLabel labelContBonus;
	private MenuButton btnBack;

	private float minTimeScreen = 0.2f;
	private float currentTimeScreen = 0f;
	private float transitionCurrentTime;
	private float transitionStateTime = 0.25f;
	private boolean activated = false;

	public HighScoresScreen(BalloonShooter game) {
		super(Assets.background);
		this.game = game;
		game.setAdsVisible(true);
		game.catchBackButton(true);
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void render(float delta) {
		super.render(delta);

		if (activated) {
			transitionCurrentTime += delta;
			if (transitionCurrentTime >= transitionStateTime) {
				game.setScreen(new MainMenuScreen(game));
			}
		}

		batch.begin();
		labelHighScoreEasy.draw(batch);
		labelHighScoreNormal.draw(batch);
		labelHighScoreHard.draw(batch);
		btnBack.draw(batch);
		batch.end();

		if (Gdx.input.isKeyPressed(Keys.BACK)) {
			game.setScreen(new MainMenuScreen(game));
		}

		if (currentTimeScreen >= minTimeScreen) {
			manageInput();
		} else {
			currentTimeScreen += delta;
		}
	}

	private void manageInput() {
		if (Gdx.input.isKeyPressed(Keys.BACK)) {
			game.setScreen(new MainMenuScreen(game));
		}
		if (Gdx.input.justTouched()) {
			Vector2 posTouched = new Vector2();
			posTouched.x = Gdx.input.getX();
			posTouched.y = GameAbstract.screenHeight - Gdx.input.getY();
			if (!activated) {
				if (btnBack.isTouchingElement(posTouched)) {
					btnBack.touch();
					activated = true;
				}
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		labelHighScoreEasy = new BasicLabel(Assets.font, "EASY: " + game.getData().getHighScoresEasy());
		labelHighScoreEasy.setPosition(BalloonShooter.screenMidX - labelHighScoreEasy.getWidth() / 2,
		      BalloonShooter.screenHeight - 3 * BalloonShooter.lblSeparationY);

		labelHighScoreNormal = new BasicLabel(Assets.font, "NORMAL: " + game.getData().getHighScoreNormal());
		labelHighScoreNormal.setPosition(BalloonShooter.screenMidX - labelHighScoreNormal.getWidth() / 2,
		      labelHighScoreEasy.getPosition().y - BalloonShooter.lblSeparationY);

		labelHighScoreHard = new BasicLabel(Assets.font, "HARD: " + game.getData().getHighScoreHard());
		labelHighScoreHard.setPosition(BalloonShooter.screenMidX - labelHighScoreHard.getWidth() / 2,
		      labelHighScoreNormal.getPosition().y - BalloonShooter.lblSeparationY);

		labelContBalloons = new BasicLabel(Assets.font, "BALLOONS: " + game.getData().getContBalloons());
		labelContBalloons.setPosition(BalloonShooter.screenMidX - labelContBalloons.getWidth() / 2,
		      labelHighScoreHard.getPosition().y - BalloonShooter.lblSeparationY);

		labelContBonus = new BasicLabel(Assets.font, "X2 BONUS: " + game.getData().getContBonus());
		labelContBonus.setPosition(BalloonShooter.screenMidX - labelContBonus.getWidth() / 2,
		      labelContBalloons.getPosition().y - BalloonShooter.lblSeparationY);

		btnBack = new MenuButton("BACK");
		btnBack.setPosition(BalloonShooter.screenMidX - btnBack.getOrigin().x,
		      labelContBonus.getPosition().y - btnBack.getHeight() - BalloonShooter.lblSeparationY);

	}

	@Override
	public void show() {
		batch = new SpriteBatch();
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		batch.dispose();
		game.dispose();
	}

}
