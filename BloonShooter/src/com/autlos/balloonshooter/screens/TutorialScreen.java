package com.autlos.balloonshooter.screens;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TutorialScreen implements Screen {
	private BalloonShooter game;

	private SpriteBatch batch;
	private int stage;
	private TextureRegion[] frames;
	private float minTime = 1.2f;
	private float currentTime;

	public TutorialScreen(BalloonShooter game) {
		this.game = game;
		BalloonShooter.doingTutorial = true;
		game.setAdsVisible(false);

		frames = new TextureRegion[2];
		frames[0] = new TextureRegion(Assets.tutorial1, 0, 0, 480, 800);
		frames[1] = new TextureRegion(Assets.tutorial2, 0, 0, 480, 800);
		
		batch = new SpriteBatch();

		currentTime = 0;
		stage = 0;
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.begin();
		batch.draw(frames[stage], 0, 0, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 1, 1, 0);
		batch.end();

		if (Gdx.input.justTouched()) {
			if (currentTime >= minTime) {
				stage++;
				currentTime = 0;
			}
		}
		if (Gdx.input.isKeyPressed(Keys.BACK) || Gdx.input.isKeyPressed(Keys.NUM_5)) {
			if (stage == 1){
				stage = 0;
				currentTime = 0;
			}else if (stage == 0) {
				goToMainMenu();
			}
		} else if (Gdx.input.isKeyPressed(Keys.HOME) || Gdx.input.isKeyPressed(Keys.NUM_6)) {
			goToMainMenu();
		}

		currentTime += delta;
		if (stage >= 2) {
			game.getData().setTutorialDone(true);
			BalloonShooter.doingTutorial = false;
			game.setScreen(new LevelSelectionScreen(game));
		}

	}

	public void goToMainMenu() {
		BalloonShooter.doingTutorial = false;
		game.setScreen(new MainMenuScreen(game));
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		batch.dispose();
	}

}
