package com.autlos.balloonshooter.screens;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.Sounds;
import com.autlos.balloonshooter.models.ui.BtnAudio;
import com.autlos.balloonshooter.models.ui.MenuButton;
import com.autlos.sgf.BasicLabel;
import com.autlos.sgf.GameAbstract;
import com.autlos.sgf.ScreenAbstract;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.swarmconnect.Swarm;

public class MainMenuScreen extends ScreenAbstract {
	private BalloonShooter game;

	private MenuButton btnPlay;
	private MenuButton btnScores;
	private BtnAudio btnAudio;
	private BasicLabel lblTitle;

	// exit
	private BasicLabel labelExit;
	private MenuButton btnExitYes;
	private MenuButton btnExitNo;
	private boolean isExitState;

	//
	private float minTimeScreen = 0.2f;
	private float currentTimeScreen = 0f;
	private float transitionCurrentTime;
	private float transitionStateTime = 0.25f;
	private boolean activated;

	public MainMenuScreen(BalloonShooter game) {
		super(Assets.background);
		this.game = game;
		game.setAdsVisible(true);
		game.catchBackButton(true);

		if (!Sounds.music.isPlaying() && BalloonShooter.soundState != Sounds.MUTED)
			game.sounds.startMusic(0.6f);

		isExitState = false;

	}

	@Override
	public void render(float delta) {
		super.render(delta);

		if (activated) {
			if (transitionCurrentTime >= transitionStateTime) {
				activated = false;
				if (btnPlay.isPressed()) {
					if (game.getData().isTutorialDone()) {
						game.setScreen(new LevelSelectionScreen(game));
					} else{
						game.setScreen(new TutorialScreen(game));
						game.getData().setTutorialDone(true);
					}
				} else if (btnScores.isPressed()) {
					if (BalloonShooter.isSwarmActive) {
						Swarm.showLeaderboards();
					} else {
						game.setScreen(new HighScoresScreen(game));
					}
				}
			} else {
				transitionCurrentTime += delta;
			}
		}

		batch.begin();

		lblTitle.draw(batch);

		if (!isExitState) {
			btnPlay.draw(batch);
			btnScores.draw(batch);
			btnAudio.draw(batch);
		} else {
			labelExit.draw(batch);
			btnExitYes.draw(batch);
			btnExitNo.draw(batch);
		}

		batch.end();

		if (currentTimeScreen < minTimeScreen) {
			currentTimeScreen += delta;
		} else {
			manageInput();
		}
	}

	private void manageInput() {
		if (Gdx.input.isKeyPressed(Keys.BACK)) {
			isExitState = true;
		}
		if (Gdx.input.justTouched()) {
			Vector2 posTouched = new Vector2();
			posTouched.x = Gdx.input.getX();
			posTouched.y = GameAbstract.screenHeight - Gdx.input.getY();
			if (!isExitState) {
				if (!activated) {
					if (btnPlay.isTouchingElement(posTouched)) {
						btnPlay.touch();
						activated = true;
					} else if (btnScores.isTouchingElement(posTouched)) {
						btnScores.touch();
						activated = true;
					} else if (btnAudio.isTouchingElement(posTouched)) {
						btnAudio.touch();
					}
					// else if (btnRate.isTouchingElement(posTouched)) {
					// Assets.playClickSound();
					// game.showRating();
					// } else if (btnTweet.isTouchingElement(posTouched)) {
					// Assets.playClickSound();
					// game.showTwitter();
					// } else if (btnFacebook.isTouchingElement(posTouched)) {
					// Assets.playClickSound();
					// game.showFacebook();
					// } else {
					// backgroundApples.add(AppleFactory.createApple());
					// }
				}

			} else {
				if (btnExitYes.isTouchingElement(posTouched)) {
					Gdx.app.exit();
				} else if (btnExitNo.isTouchingElement(posTouched)) {
					isExitState = false;
				}
			}
		}
	}

	@Override
	public void resize(int width, int height) {
	
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(null);
		
		lblTitle = new BasicLabel(Assets.font, BalloonShooter.TITLE);
		lblTitle.setPosition(BalloonShooter.screenMidX - lblTitle.getWidth() / 2, BalloonShooter.screenHeight - 3
		      * BalloonShooter.lblSeparationY);

		btnPlay = new MenuButton("PLAY");
		btnPlay.setPosition(BalloonShooter.screenMidX - btnPlay.getOrigin().x,
		      lblTitle.getPosition().y - btnPlay.getHeight() - BalloonShooter.lblSeparationY);

		btnScores = new MenuButton("RECORDS");
		btnScores.setPosition(BalloonShooter.screenMidX - btnScores.getOrigin().x,
		      btnPlay.getPosition().y - btnScores.getHeight() - BalloonShooter.lblSeparationY);

		btnAudio = new BtnAudio(new Vector2());
		btnAudio.setPosition(BalloonShooter.screenMidX - btnAudio.getOrigin().x, btnScores.getPosition().y
		      - BalloonShooter.lblSeparationY - btnAudio.getHeight());

		// EXIT STATE
		labelExit = new BasicLabel(Assets.font, "EXIT?");
		labelExit.setPosition(GameAbstract.screenMidX - labelExit.getWidth() / 2, GameAbstract.screenMidY + 200
		      * BalloonShooter.scaleY);

		btnExitYes = new MenuButton("YES");
		btnExitYes.setPosition(GameAbstract.screenMidX - btnExitYes.getOrigin().x,
		      labelExit.getPosition().y - btnExitYes.getHeight() - BalloonShooter.lblSeparationY);

		btnExitNo = new MenuButton("NO");
		btnExitNo.setPosition(GameAbstract.screenMidX - btnExitNo.getOrigin().x,
		      btnExitYes.getPosition().y - btnExitNo.getHeight() - BalloonShooter.lblSeparationY);

		activated = false;

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		super.dispose();
	}

}
