package com.autlos.balloonshooter.models;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.sgf.models.MoveableEntity;
import com.badlogic.gdx.math.Vector2;

public class Bonus extends MoveableEntity {
	private boolean direction;
	// This is to describe a zig zag movement on the Y axys:
	private float maxY;
	private float minY;
	private boolean goingUp;
	public Bonus(float SPEED, Vector2 position) {
		super(Assets.bonusx2, position, SPEED, BalloonShooter.scaleX, BalloonShooter.scaleY, 0f);
		
				
	   maxY = position.y + 20f*BalloonShooter.scaleY;
	   minY = position.y - 20f*BalloonShooter.scaleY;
	   direction = this.position.x < 0 ? true : false;
	   
	   goingUp = true;
	   
	   this.createBounds(3f, 3f);
	   
   }
	
	public void update(float delta){
		if(goingUp){
			if(position.y <= maxY){
				position.y += SPEED*0.40f*delta;
			}else{
				goingUp = false;
			}
		}else{
			if(position.y >= minY){
				position.y -= SPEED*0.40f*delta;
			}else{
				goingUp = true;
			}
		}
		
		if(direction){
			this.position.x += SPEED*delta;
		}else{
			this.position.x -= SPEED*delta;
		}
		super.update();
	}

}
