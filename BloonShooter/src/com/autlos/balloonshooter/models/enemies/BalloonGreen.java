package com.autlos.balloonshooter.models.enemies;

import com.autlos.balloonshooter.Assets;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class BalloonGreen extends Balloon {

	float minPosX;
	float maxPosX;
	float SPEED_X;
	private boolean goingLeft;
	public BalloonGreen(Vector2 position, float SPEED) {
		super(Assets.greenBalloon, position, SPEED, 64);
		
		minPosX = position.x - 25f*scaleX;
		maxPosX = position.x + 25f*scaleX;
		goingLeft = MathUtils.randomBoolean();
		
		this.points = 200;
	}

	public void update(float delta) {
		if(goingLeft){
			if(position.x <= maxPosX){
				position.x += SPEED*0.8f*delta;
			}else{
				goingLeft = false;
			}
		}else{
			if(position.x >= minPosX){
				position.x -= SPEED*0.8f*delta;
			}else{
				goingLeft = true;
			}
		}
		
		super.update(delta);
	}

}
