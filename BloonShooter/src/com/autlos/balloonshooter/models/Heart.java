package com.autlos.balloonshooter.models;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.badlogic.gdx.math.Vector2;

public class Heart extends com.autlos.sgf.models.Entity {
	// Min time before it can be destroyed (this way it's not taken immeadiately with a rock)
	private float minLife = 0.6f;
	// If the currentTime is > maxLifeTime, the heart disappears.
	private float currentTime;
	private float maxLifeTime = 2.5f;
	// Tells the game that this heart has to disappear.
	private boolean finished;

	public Heart(Vector2 position) {
		super(Assets.heart, position, 0, BalloonShooter.scaleX, BalloonShooter.scaleX);
		this.createBounds(1f, 4f);

		currentTime = 0f;
		finished = false;
	}

	public boolean isFinished() {
		return finished;
	}

	public float getMinTime() {
		return minLife;
	}

	public void update(float delta) {
		if (minLife >= 0f)
			minLife -= delta;
		else {
			currentTime += delta;
		}

		if (currentTime > maxLifeTime) {
			finished = true;
		}

	}

}
