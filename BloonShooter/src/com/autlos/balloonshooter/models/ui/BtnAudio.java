package com.autlos.balloonshooter.models.ui;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.Sounds;
import com.autlos.sgf.GuiElement;
import com.badlogic.gdx.math.Vector2;

public class BtnAudio extends GuiElement {

	public BtnAudio(Vector2 position) {
		super(Assets.btnAudio, position, 1, 2, BalloonShooter.scaleX, BalloonShooter.scaleY);
		createBounds(-12f, -8f);
		if (BalloonShooter.soundState == Sounds.NOT_MUTED) {
			this.setState(State.NOT_PRESSED);
		} else {
			this.setState(State.PRESSED);
		}
	}

	public void setPosition(Vector2 position) {
		super.setPosition(position);
		this.createBounds(-2f, -2f);

	}
	
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
		this.createBounds(-2f, -2f);

	}

	public void touch() {
		this.switchState();
		Sounds.changeState();
	}

}
