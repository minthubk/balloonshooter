package com.autlos.balloonshooter.models;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonFactory.DifficultyMode;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.models.weapons.Bullet;
import com.autlos.balloonshooter.models.weapons.SlowRock;
import com.autlos.sgf.models.BasicProjectile;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Shooter extends com.autlos.sgf.models.Entity {
	public static final int WEAPON_1 = 1;
	public static final int WEAPON_2 = 2;

	private float RELOAT_TIME_ROCK;
	private float RELOAD_TIME_BULLET;
	private float SPEED_BULLET;
	private float SPEED_ROCK;

	private int currentWeapon;
	private boolean shooting;
	private float reloadTime;
	private float stateTime;
	
	private float proyectilePosY;

	/**
	 * 
	 * @param position
	 * @param scale
	 * @param df
	 *           DifficultyMode
	 */
	public Shooter(Vector2 position) {
		super(Assets.shooter, 1, 2, position, 0f, BalloonShooter.scaleX, BalloonShooter.scaleX);
		
		proyectilePosY = position.y + 15 * BalloonShooter.scaleX;
		
		currentWeapon = 1;

	}

	public void setProjectileConsts(DifficultyMode df) {
		switch (df) {
		case HARD:
			RELOAD_TIME_BULLET = 0.65f;
			RELOAT_TIME_ROCK = 1.10f;
			SPEED_BULLET = Gdx.graphics.getHeight() / 2.5f;
			SPEED_ROCK = Gdx.graphics.getHeight() / 4.5f;
			break;
		case NORMAL:
			RELOAD_TIME_BULLET = 0.6f;
			RELOAT_TIME_ROCK = 1.0f;
			SPEED_BULLET = Gdx.graphics.getHeight() / 2.3f;
			SPEED_ROCK = Gdx.graphics.getHeight() / 4.2f;
			break;
		case EASY:
			RELOAD_TIME_BULLET = 0.50f;
			RELOAT_TIME_ROCK = 0.90f;
			SPEED_BULLET = Gdx.graphics.getHeight() / 2.0f;
			SPEED_ROCK = Gdx.graphics.getHeight() / 4.0f;
			break;

		}

	}

//	public void draw(SpriteBatch batch) {
//		sprite.draw(batch);
//	}

	/**
	 * Updates the reload time and the region to be drawn
	 */
	public void update(float delta) {
		if (shooting) {
			stateTime += delta;
		}

		if (stateTime >= reloadTime) {
			currentFrame = frames[0];
			stateTime = 0f;
			shooting = false;
		}

	}

	public int getWeapon() {
		return currentWeapon;
	}

	public void setWeapon(int WEAPON_NUMBER) {
		this.currentWeapon = WEAPON_NUMBER;
	}

	public float getRotation() {
		return rotation;
	}

	public void setRotation(float rotation) {
		this.rotation = rotation - 90;
		
	}

	public BasicProjectile shoot() {
		shooting = true;
		currentFrame = frames[1];

		BasicProjectile p = null;
		
		switch (currentWeapon) {
		case WEAPON_1:
			p = new Bullet(Assets.bullet, SPEED_BULLET, new Vector2(), rotation + 90);
			p.setPosition(getOriginCoordinates().x - p.getOrigin().x, proyectilePosY - p.getOrigin().y);
			reloadTime = RELOAD_TIME_BULLET;
			break;
		case WEAPON_2:
			reloadTime = RELOAT_TIME_ROCK;
			p = new SlowRock(Assets.slowRock, SPEED_ROCK, new Vector2(), rotation + 90);
			p.setPosition(getOriginCoordinates().x - p.getOrigin().x, proyectilePosY - p.getOrigin().y);
		}

		return p;
	}

	public boolean isShooting() {
		return shooting;
	}

}
