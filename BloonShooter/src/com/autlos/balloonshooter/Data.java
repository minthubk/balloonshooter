package com.autlos.balloonshooter;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import com.autlos.balloonshooter.SwarmConsts.Achievement;
import com.autlos.balloonshooter.SwarmConsts.Leaderboard;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.swarmconnect.SwarmAchievement;
import com.swarmconnect.SwarmLeaderboard;

/**
 * This is the worst class to manage files ever created. I'm so sorry about this. Just look at the achievements
 * 
 * @author Aitor
 * 
 */
public class Data {

	private String highScoreEasy;
	private String highScoreHard;
	private String highScoreNormal;
	private String contGlobalBalloons;
	private String contBlues;
	private String contGames;
	private String contBonus;
	private String contHearts;
	private boolean tutorialDone;
	
	// THIS game's balloons count.
	private int contGameBalloons;

	private FileHandle fileScores;

	public Data() {
		contGameBalloons = 0;
		// Scores:		
		fileScores = Gdx.files.local("properties.txt");
		try {
			if (fileScores.exists()) {
				readScores();
			} else {
				writeScores(); 
			}
		} catch (Exception e) {
			writeScores();
			Gdx.app.log("Error", "couldn't create file handle.");
			e.printStackTrace();
		}

	}

	/**
	 * Intialize the variables when it's the first time played.
	 * Sometimes the game crashes due to file updates, this weird method may solve that.
	 */
	private void fixNulls() {
		if (highScoreEasy == null)
			highScoreEasy = "0";
		if (highScoreNormal == null)
			highScoreNormal = "0";
		if (highScoreHard == null)
			highScoreHard = "0";
		if (contGlobalBalloons == null)
			contGlobalBalloons = "0";
		if (contBonus == null)
			contBonus = "0";
		if (contBlues == null)
			contBlues = "0";
		if (contGames == null)
			contGames = "0";
		if( contHearts == null)
			contHearts = "0";
	}

	/**
	 * Read the past games scores.
	 */
	public void readScores() {
		String lines[];
		lines = fileScores.readString().split(":");

		// First version of the file
		tutorialDone = Boolean.parseBoolean(lines[0]);
		highScoreNormal = lines[1];
		highScoreHard = lines[2];

		// Conditionals below are to avoid crashes when updating the game. Hard to explain, just don't ever do things like this.
		// second version of the file
		if (lines.length < 4) {
			contGlobalBalloons = "0";
		} else {
			contGlobalBalloons = lines[3];
		}
		if(lines.length < 5){
			contBonus = "0";			
		}else{
			contBonus = lines[4];			
		}
		
		// third version of the file
		if (lines.length < 6) {
			contBlues = "0";
		} else {
			contBlues = lines[5];
		}
		
		if(lines.length < 7){
			contGames = "0";			
		}else{			
			contGames = lines[6];
		}
		
		// fourth version of the file
		if (lines.length < 8) {
			highScoreEasy = "0";
		} else {
			highScoreEasy = lines[7];
		}
		
		// Fifth version of the file
		if(lines.length < 9){
			contHearts = "0";
		}else{
			contHearts = lines[8];
		}

	}

	/**
	 * Create/save scores
	 */
	public void writeScores() {		
		// In case it's the first time the game is played
		fixNulls();
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(fileScores.write(false)));
			bw.write(Boolean.toString(tutorialDone) + ":");
			bw.write(highScoreNormal + ":");
			bw.write(highScoreHard + ":");
			bw.write(contGlobalBalloons + ":");
			bw.write(contBonus + ":");
			bw.write(contBlues + ":");
			bw.write(contGames + ":");
			bw.write(highScoreEasy + ":");
			bw.write(contHearts);
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

	public String getContBlues() {
		return contBlues;
	}

	/**
	 * Adds blue balloons escaped to check the "turtle" achievement
	 * 
	 * @param contBlues
	 */
	public void addContBlues() {
		int aux = Integer.parseInt(this.contBlues);
		aux += 1;
		setContBlues(aux);
	}

	private void setContBlues(int contBlues) {
		this.contBlues = Integer.toString(contBlues);
		if (contBlues >= 10) {
			unlockAchievement(Achievement.TURTLE_ID);
		}
	}

	public String getContGames() {
		return contGames;
	}

	public void addContGames() {
		int aux = Integer.parseInt(this.contGames);
		aux += 1;
		setContGames(aux);
	}

	public void setContGames(int contGames) {
		this.contGames = Integer.toString(contGames);
		// Achievements for playing 5,20 or 50 cames
		if (contGames >= 5) {
			unlockAchievement(Achievement.CONSTANT_I_ID);
			if (contGames >= 20) {
				unlockAchievement(Achievement.CONSTANT_II_ID);
				if (contGames >= 50) {
					unlockAchievement(Achievement.CONSTANT_III_ID);
				}
			}
		}
	}
	
	public int getContHearts(){
		return Integer.parseInt(contHearts);
	}
	
	public void addContHearts(){
		int aux = Integer.parseInt(contHearts);
		aux += 1;
		setContHearts(aux);
	}
	
	public void setContHearts(int contHearts){
		this.contHearts = Integer.toString(contHearts);
		// Achievements for taking 10, 25, 50 hearts
		if(contHearts >= 10){
			unlockAchievement(Achievement.SURVIVOR_LVL_I_ID);
			if(contHearts >= 25){
			 unlockAchievement(Achievement.SURVIVOR_LVL_II_ID);
			 if(contHearts >= 50){
				 unlockAchievement(Achievement.SURVIVOR_LVL_III_ID);
			 }
			}
		}
		
	}

	public int getContBonus() {
		return Integer.parseInt(contBonus);
	}

	public void addContBonus() {
		int aux = Integer.parseInt(this.contBonus);
		aux += 1;
		setContBonus(aux);
	}

	private void setContBonus(int contBonus) {
		this.contBonus = Integer.toString(contBonus);
		// Achievements for taking 10,25,50 bonuses
		if (contBonus >= 10) {
			unlockAchievement(Achievement.LUCK_OR_PRECISION_I_ID);
			if (contBonus >= 25) {
				unlockAchievement(Achievement.LUCK_OR_PRECISION_II_ID);
				if (contBonus >= 50) {
					unlockAchievement(Achievement.LUCK_OR_PRECISION_III_ID);
				}
			}
		}
	}

	public int getContBalloons() {
		return Integer.parseInt(contGlobalBalloons);
	}

	public void addContBalloons() {
		contGameBalloons++;
		// Check THIS game's contBallons in order to unlock these 3 achievements:
		if (contGameBalloons >= 50) {
			unlockAchievement(Achievement.BALLOON_MANIAC_I_ID);
			if (contGameBalloons >= 100) {
				unlockAchievement(Achievement.BALLOON_MANIAC_II_ID);
				if (contGameBalloons >= 200) {
					unlockAchievement(Achievement.BALLOON_MANIAC_III_ID);
				}
			}
		}
		
	}

	/**
	 * Save the global count when a game is finished
	 * @param contGameBalloons
	 */
	public void setContballoons() {
		// global count + this game's count:
		int newScore = contGameBalloons + Integer.parseInt(contGlobalBalloons);
		contGlobalBalloons = Integer.toString(newScore);
		// Submit the total balloons killed to the leaderboard
		submitScore(Leaderboard.BALLOONS_COUNT_ID, Float.parseFloat(contGlobalBalloons));

		if (newScore >= 250) {
			unlockAchievement(Achievement.BRONZE_KILLER_ID);
			if (newScore >= 1000) {
				unlockAchievement(Achievement.SILVER_KILLER_ID);
				if (newScore >= 2500) {
					unlockAchievement(Achievement.GOLD_KILLER_ID);
					if (newScore >= 5000) {
						unlockAchievement(Achievement.DIAMOND_KILLER_ID);
					}
				}
			}
		}
		// Reset the count for next games:
		contGameBalloons = 0;
	}

	public String getHighScoreHard() {
		return highScoreHard;
	}

	public void setHighScoreHard(long highScoreHard) {
		if (highScoreHard > Long.parseLong(this.highScoreHard)) {
			this.highScoreHard = Long.toString(highScoreHard);
		}
		// Submit the score to the leaderboard
		submitScore(Leaderboard.HIGH_SCORES_HARD_ID, highScoreHard);
		// Achievements:
		if (highScoreHard >= 25000) {
			unlockAchievement(Achievement.HARD_APPRENTICE_ID);
			if (highScoreHard >= 50000) {
				unlockAchievement(Achievement.HARD_AMATEUR_ID);
				if (highScoreHard >= 100000) {
					unlockAchievement(Achievement.HARD_VETERAN_ID);
					if (highScoreHard >= 250000)
						unlockAchievement(Achievement.HARD_MASTER_ID);
				}
			}
		}
	}

	public String getHighScoresEasy() {
		return highScoreEasy;
	}

	public void setHighScoresEasy(long highScoresEasy) {
		if (highScoresEasy > Long.parseLong(highScoreEasy)) {
			this.highScoreEasy = Long.toString(highScoresEasy);
		}
		// Submit the score to the leaderboard
		submitScore(SwarmConsts.Leaderboard.HIGH_SCORES_EASY_ID, highScoresEasy);
		// Achievements
		if (highScoresEasy > 25000) {
			unlockAchievement(SwarmConsts.Achievement.EASY_APPRENTICE_ID);
			if (highScoresEasy > 100000) {
				unlockAchievement(SwarmConsts.Achievement.EASY_AMATEUR_ID);
				if (highScoresEasy > 500000) {
					unlockAchievement(SwarmConsts.Achievement.EASY_VETERAN_ID);
					if (highScoresEasy > 1000000)
						unlockAchievement(SwarmConsts.Achievement.EASY_MASTER_ID);
				}
			}
		}

	}

	public String getHighScoreNormal() {
		return highScoreNormal;
	}

	public void setHighScoreNormal(long highScoreNormal) {
		if (highScoreNormal > Long.parseLong(this.highScoreNormal)) {
			this.highScoreNormal = Long.toString(highScoreNormal);
		}
		// Leaderboard:
		submitScore(Leaderboard.HIGH_SCORES_NORMAL_ID, highScoreNormal);
		// Check achievements:
		if (highScoreNormal >= 25000) {
			unlockAchievement(Achievement.NORMAL_APPRENTICE_ID);
			if (highScoreNormal >= 100000) {
				unlockAchievement(Achievement.NORMAL_AMATEUR_ID);
				if (highScoreNormal >= 500000) {
					unlockAchievement(Achievement.NORMAL_VETERAN_ID);
					if (highScoreNormal >= 1000000)
						unlockAchievement(Achievement.NORMAL_MASTER_ID);
				}
			}
		}
	}

	/**
	 * Unlock achievement only if isSwarmActive
	 * 
	 * @param key
	 */
	public void unlockAchievement(int key) {
		if (BalloonShooter.isSwarmActive) {
			SwarmAchievement.unlock(key);
		}
	}

	/**
	 * Submit score to a leaderBoard only if isSwarmActive
	 * 
	 * @param leaderBoard
	 * @param value
	 */
	public void submitScore(int leaderBoard, float value) {
		if (BalloonShooter.isSwarmActive) {
			SwarmLeaderboard.submitScore(leaderBoard, value);
		}
	}

	public boolean isTutorialDone() {
		return tutorialDone;
	}

	public void setTutorialDone(boolean tutorialDone) {
		this.tutorialDone = tutorialDone;
	}

}
