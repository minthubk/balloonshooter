package com.autlos.balloonshooter;

import com.autlos.sgf.GameAbstract.TARGET_SIZE;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {
	private TextureAtlas atlasImages;

	// Regions
	public static TextureRegion splashTexture;
	public static TextureRegion heart;
	public static TextureRegion redBalloon;
	public static TextureRegion greenBalloon;
	public static TextureRegion blueBalloon;
	public static TextureRegion smallBalloon;
	public static TextureRegion slowRock;
	public static TextureRegion bullet;
	public static TextureRegion bonusx2;
	public static TextureRegion background;
	public static TextureRegion tutorial1;
	public static TextureRegion tutorial2;
	public static TextureRegion shooter;
	public static TextureRegion btnWeapon;
	public static TextureRegion btnAudio;
	public static TextureRegion btnMenu;

	// Fonts
	public static BitmapFont font;

	public Assets() {
		atlasImages = new TextureAtlas("data/textures800-480.pack");

		loadTextures();
		loadFonts();
	}

	private void loadTextures() {
		if (BalloonShooter.targetSize == TARGET_SIZE.BIG) {
			splashTexture = atlasImages.findRegion("splashBig");
		} else {
			splashTexture = atlasImages.findRegion("splashSmall");
		}

		slowRock = atlasImages.findRegion("rock");
		bullet = atlasImages.findRegion("bullet");
		bonusx2 = atlasImages.findRegion("bonusx2");
		redBalloon = atlasImages.findRegion("balloonred");
		greenBalloon = atlasImages.findRegion("balloongreen");
		smallBalloon = atlasImages.findRegion("balloonsmall");
		blueBalloon = atlasImages.findRegion("balloonblue");
		heart = atlasImages.findRegion("heart");
		background = atlasImages.findRegion("background");
		btnMenu = atlasImages.findRegion("btnmenu");
		tutorial1 = atlasImages.findRegion("tutorial1");
		tutorial2 = atlasImages.findRegion("tutorial2");
		shooter = atlasImages.findRegion("shooter");
		btnWeapon = atlasImages.findRegion("btnweapon");
		btnAudio = atlasImages.findRegion("btnaudio");

	}

	private void loadFonts() {
		if (BalloonShooter.targetSize == TARGET_SIZE.BIG) {
			font = new BitmapFont(Gdx.files.internal("data/font33.fnt"), false);
			font.setScale(BalloonShooter.SCALE_X_BIG, BalloonShooter.SCALE_Y_BIG);
			Gdx.app.log("font chosen", "big, scale: " + font.getScaleX() + ":" + font.getScaleY());
		} else {
			font = new BitmapFont(Gdx.files.internal("data/font28.fnt"), false);
			font.setScale(BalloonShooter.SCALE_X_SMALL, BalloonShooter.SCALE_Y_SMALL);
			Gdx.app.log("font chosen", "small, scale: " + font.getScaleX() + ":" + font.getScaleY());
		}
		font.setColor(Color.WHITE);
	}

	public void dispose() {
		atlasImages.dispose();
	}

}
