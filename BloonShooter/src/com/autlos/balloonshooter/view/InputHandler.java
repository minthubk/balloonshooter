package com.autlos.balloonshooter.view;

import com.autlos.balloonshooter.Sounds;
import com.autlos.balloonshooter.models.Shooter;
import com.autlos.balloonshooter.screens.GameOverScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class InputHandler implements InputProcessor {

	World world;

	public InputHandler(World world) {
		this.world = world;
	}

	@Override
	public boolean keyDown(int keycode) {
		// Nums are for keyboard use (desktop version)
		if (keycode == Keys.NUM_1) {
			world.getShooter().setWeapon(Shooter.WEAPON_1);
		}
		if (keycode == Keys.NUM_2) {
			world.getShooter().setWeapon(Shooter.WEAPON_2);
		}

		// I assigned num5 as a way to finish a game (debug purposes)
		if (keycode == Keys.BACK || keycode == Keys.NUM_5) {
			world.getGame().setScreen(new GameOverScreen(world));
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// If you don't understand this, I highly recommend you to study some elemental trigonometry
		float distX, distY;
		distX = screenX - Gdx.graphics.getWidth() / 2;
		distY = (Gdx.graphics.getHeight() - screenY) - world.posShooterY;
		float rotation = MathUtils.atan2(distY, distX) * (360 / (2 * MathUtils.PI));

		// I create a 1x1 rectangle in the position touched to check if it's touching a game button
		// There is no other way as the game buttons are just images.
		float posTouchedY = Gdx.graphics.getHeight() - screenY;
		
		Vector2 posTouched = new Vector2(new Vector2(screenX, posTouchedY));
		if (world.getBtnWeapon().isTouchingElement(posTouched)) {
			// If it's touching the weapon button
			world.getBtnWeapon().switchState();
			if (world.getShooter().getWeapon() == Shooter.WEAPON_1) {
				world.getShooter().setWeapon(Shooter.WEAPON_2);
			} else {
				world.getShooter().setWeapon(Shooter.WEAPON_1);
			}

		} else if (world.getBtnSound().isTouchingElement(posTouched)) {
			// If it's touching the sound button
			world.getBtnSound().switchState();
			Sounds.changeState();
		} else {
			// This are the min and max angles for the shoot
			if (rotation < 30) {
				world.setRotation(30);
			} else if (rotation > 150) {
				world.setRotation(150);
			} else {
				world.setRotation(rotation);
			}
			world.shoot();
		}

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// This method is only for the desktop version
		float distX, distY;
		distX = screenX - Gdx.graphics.getWidth() / 2;
		distY = (Gdx.graphics.getHeight() - screenY) - world.posShooterY;

		float rotation = MathUtils.atan2(distY, distX) * (360 / (2 * MathUtils.PI));
		if (rotation > 30 && rotation < 150) {
			world.setRotation(rotation);
		}
		return true;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
