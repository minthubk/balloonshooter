package com.autlos.balloonshooter.view;

import com.autlos.balloonshooter.Assets;
import com.autlos.balloonshooter.BalloonFactory;
import com.autlos.balloonshooter.BalloonFactory.BalloonType;
import com.autlos.balloonshooter.BalloonShooter;
import com.autlos.balloonshooter.Sounds;
import com.autlos.balloonshooter.SwarmConsts;
import com.autlos.balloonshooter.models.Bonus;
import com.autlos.balloonshooter.models.Heart;
import com.autlos.balloonshooter.models.LabelPoints;
import com.autlos.balloonshooter.models.Shooter;
import com.autlos.balloonshooter.models.enemies.Balloon;
import com.autlos.balloonshooter.models.enemies.BalloonBlue;
import com.autlos.balloonshooter.models.enemies.BalloonRed;
import com.autlos.balloonshooter.models.enemies.BalloonSmall;
import com.autlos.balloonshooter.models.ui.BtnAudio;
import com.autlos.balloonshooter.models.ui.BtnWeapon;
import com.autlos.balloonshooter.models.weapons.Bullet;
import com.autlos.balloonshooter.models.weapons.SlowRock;
import com.autlos.sgf.BasicLabel;
import com.autlos.sgf.GuiElement;
import com.autlos.sgf.models.BasicProjectile;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class World {
	BalloonShooter game;

	// Game entities:
	protected Shooter shooter;
	protected BtnWeapon btnWeapon;
	protected BtnAudio btnSound;
	protected Array<BasicProjectile> projectiles;
	protected Array<Balloon> balloons;
	protected Array<Heart> hearts;
	protected Array<LabelPoints> aLabelPoints;
	protected Bonus bonus;

	// Game variables such as the highScore and the current score:
	protected BasicLabel labelScore;
	protected BasicLabel labelBest;

	protected BalloonFactory balloonFactory;
	protected long highScore;
	protected long score;
	protected int contScore;
	protected boolean isBonus;
	protected int lifes;
	protected float posShooterY;

	public World(BalloonShooter game) {
		this.game = game;
		// Y axys position for the shooter, 100px * scale is the standard to avoid the admob banner:
		posShooterY = 100f * BalloonShooter.scaleY;

		// Create entities:
		btnWeapon = new BtnWeapon();
		btnSound = new BtnAudio(new Vector2(Gdx.graphics.getWidth() - 143 * BalloonShooter.scaleX * 0.5f,
		      95f * BalloonShooter.scaleY));
		shooter = new Shooter(new Vector2(Gdx.graphics.getWidth() / 2 - 16, posShooterY));
		projectiles = new Array<BasicProjectile>();
		balloons = new Array<Balloon>();
		aLabelPoints = new Array<LabelPoints>();

		hearts = new Array<Heart>();

		// Game:
		score = 0;

		// Input processor:
		Gdx.input.setInputProcessor(new InputHandler(this));

		// Initialize labels:
		labelBest = new BasicLabel(Assets.font, "BEST: ");
		labelBest.setPosition(10f * BalloonShooter.scaleX, BalloonShooter.screenHeight - BalloonShooter.lblSeparationY);

		labelScore = new BasicLabel(Assets.font, "SCORE: ");
		labelScore.setPosition(10f * BalloonShooter.scaleX, labelBest.getPosition().y - labelScore.getHeight());
	}

	public void update(float delta) {
		labelScore.setValue(score);

		shooter.update(delta);
		checkCollisions();

		// Check if the projectile has disappeared to remove it
		for (BasicProjectile iterP : projectiles) {
			if (iterP.getPosition().x > Gdx.graphics.getWidth() || iterP.getPosition().x + iterP.getWidth() < 0
			      || iterP.getPosition().y - iterP.getHeight() > Gdx.graphics.getHeight()) {
				projectiles.removeValue(iterP, false);
			} else {
				iterP.update(delta);
			}
		}

		// Update the hearts and remove them if needed (this is a just in case
		// because it is almost impossible to have 2 hearts at the same time)
		for (Heart iterH : hearts) {
			iterH.update(delta);
			if (iterH.isFinished()) {
				hearts.removeValue(iterH, false);
			}
		}

		for (LabelPoints iterL : aLabelPoints) {
			if (iterL.isFinished()) {
				aLabelPoints.removeValue(iterL, false);
			} else {
				iterL.update(delta);
			}
		}
	}

	/**
	 * Check the collisiones between projectiles and everything
	 */
	public void checkCollisions() {
		for (BasicProjectile iterP : projectiles) {
			if (isBonus) {
				if (iterP.getBounds().overlaps(bonus.getBounds())) {
					// Removes the bonus, multiply the score by 2 and add one to the bonus count (for achievements)
					isBonus = false;
					bonus = null;
					score = score * 2;
					game.getData().addContBonus();
				}
			}

			for (Heart iterH : hearts) {
				if (iterP.getBounds().overlaps(iterH.getBounds()) && iterH.getMinTime() <= 0) {
					hearts.removeValue(iterH, false);
					// I thought the best thing was having a maximum of 5 lifes.
					if (lifes < 5) {
						lifes++;
					}

					// 250 points is a nice bonus for taken a heart even if you have 5 lifes
					score += 250;
					game.getData().addContHearts();
					aLabelPoints.add(new LabelPoints(250, iterH.getOriginCoordinates()));
				}
			}

			for (Balloon iterB : balloons) {
				// If the balloon is not destroyed and there is a collision
				if (!iterB.isDestroyed() && iterB.getMinLife() <= 0f && iterP.getBounds().overlaps(iterB.getBounds())) {
					// To better control the red life time after a blue balloon is destroyed:
					float newRedLifeTime = 0f;
					if (iterP instanceof SlowRock) {
						// if the projectile was a rock, apply the bonus for consecutive kills
						SlowRock p = (SlowRock) iterP;
						iterB.setPoints(iterB.getPoints() + p.getKills() * 100);
						p.addKill();
						newRedLifeTime = 0.9f;

						// If one rock has killed 5 balloons, unlock the rock proficency achievement
						if (p.getKills() >= 5) {
							game.getData().unlockAchievement(SwarmConsts.Achievement.ROCK_PROFICENCY_ID);
						}

					} else if (iterP instanceof Bullet) {
						projectiles.removeValue(iterP, false);
						newRedLifeTime = 0.25f;
					}
					// Increase the base score, global score, destroys the balloon and add count it:
					contScore += iterB.getPoints();
					score += iterB.getPoints();
					iterB.destroy();
					// contBalloons++;
					game.getData().addContBalloons();
					if (iterB instanceof BalloonBlue) {
						// If the balloon was a blue one, create a red one in it's position:
						BalloonRed rb = (BalloonRed) balloonFactory.createBalloon(BalloonType.RED, iterB.getPosition());
						rb.setMinLife(newRedLifeTime);
						balloons.add(rb);
					} else if (iterB instanceof BalloonSmall) {
						// If the balloon was a small one, create a heart
						if (mayCreateHeart())
							hearts.add(new Heart(iterB.getPosition()));
					}

					aLabelPoints.add(new LabelPoints(iterB.getPoints(), iterB.getOriginCoordinates()));
					Sounds.bloonPop();
				}
			}
		}
	}

	/**
	 * Chances to create a heart
	 * 
	 * @return
	 */
	private boolean mayCreateHeart() {
		int n = MathUtils.random(0, 4);
		// chances == 2/5.
		return (n == 1 || n == 3);
	}

	public void shoot() {
		if (!shooter.isShooting()) {
			BasicProjectile p = shooter.shoot();
			Sounds.shoot();
			projectiles.add(p);
		}

	}

	public BasicLabel getLabelScore() {
		return labelScore;
	}

	public BasicLabel getLabelBest() {
		return labelBest;
	}

	public void dispose() {

	}

	public long getHighScore() {
		return highScore;
	}

	public Shooter getShooter() {
		return shooter;
	}

	public Array<Heart> getHearts() {
		return hearts;
	}

	public Array<BasicProjectile> getProjectiles() {
		return projectiles;
	}

	public Array<Balloon> getBloons() {
		return balloons;
	}

	public Array<LabelPoints> getLabelPoints() {
		return aLabelPoints;
	}

	public long getScore() {
		return score;
	}

	public Bonus getBonus() {
		return bonus;
	}

	public boolean isBonus() {
		return isBonus;
	}

	public float getRotation() {
		return shooter.getRotation();
	}

	public void setRotation(float rotation) {
		shooter.setRotation(rotation);
	}

	public int getLifes() {
		return lifes;
	}

	public void setLifes(int lifes) {
		this.lifes = lifes;
	}

	public GuiElement getBtnWeapon() {
		return btnWeapon;
	}

	public GuiElement getBtnSound() {
		return btnSound;
	}

	public BalloonShooter getGame() {
		return game;
	}

}
