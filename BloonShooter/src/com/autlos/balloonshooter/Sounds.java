package com.autlos.balloonshooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class Sounds {
	public final static int NOT_MUTED = 1;
	public final static int MUTED = 0;

	private static Sound balloonPop;
	private static Sound shoot;
	public static Music music;

	public Sounds() {
		balloonPop = Gdx.audio.newSound(Gdx.files.internal("data/bloonpop.wav"));
		shoot = Gdx.audio.newSound(Gdx.files.internal("data/shoot.wav"));
		music = Gdx.audio.newMusic(Gdx.files.internal("data/pitx.mp3"));
		music.setLooping(true);
	}
	
	public void startMusic(float volume){
		music.setVolume(volume);
		music.play();
	}
	
	public static void stopMusic(){
		music.stop();
	}
	
	public static void shoot() {
		if (BalloonShooter.soundState == NOT_MUTED)
			shoot.play(0.25f);
	}

	public static void bloonPop() {
		if (BalloonShooter.soundState == NOT_MUTED)
			balloonPop.play(1.0f);
	}

	public void dispose() {
		balloonPop.dispose();
		shoot.dispose();
	}

	public static void changeState() {
		if (BalloonShooter.soundState == MUTED) {
			BalloonShooter.soundState = NOT_MUTED;
			music.play();
		} else {
			BalloonShooter.soundState = MUTED;
			music.pause();
		}
	}

}
